# PI Test

This site is intended to provide an easy way to test and debug the PI
javascript parsing delay issue in an isolated environment. Use the links
below to load the same set of embedded charts from the two PI
installations we're currently running.

## Running the test

The HTML files must be served from http://locahost:3000 since our PI installations have whitelisted that origin.

You can use any system you like to do this, however we'd recommend the [Serve CLI](https://www.npmjs.com/package/serve) for it's simplicity.

If you have NPM installed on your system you should simply be able to run the following command from within this directory:

```
$ npx serve -l 3000
```
