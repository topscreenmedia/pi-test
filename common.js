window.addEventListener("DOMContentLoaded", init);
var startTime;

function init() {
  document.getElementById("submit").addEventListener("click", activateIframe);
}

function activateIframe() {
  var eContainer = document.getElementById("container");
  var eMessage = document.getElementById("message");
  var password = document.getElementById("password").value;

  if (!password.length) {
    return (eMessage.textContent = "Please enter a password");
  } else {
    eMessage.textContent = "Loading token";
  }

  getToken(password)
    .then((token) => {
      eMessage.textContent = "Token loaded.";
      return startSession(token);
    })
    .then(() => {
      startTime = performance.now();
      performance.mark("start iframe load");
      eMessage.textContent = "Loading iframes...";

      for (var i in window.config.charts) {
        var eFrame = document.createElement("iframe");
        eFrame.addEventListener("load", handleIframeLoad);

        eFrame.src =
          window.config.analyticsHost +
          "/pi/chart#chart-filter/" +
          window.config.charts[i] +
          "__1";

        eContainer.appendChild(eFrame);
      }
    })
    .catch((error) => {
      eMessage.textContent = error.message;
    });
}

function getToken(password) {
  return fetch(
    window.config.analyticsHost + "/panMISDashboardWebServices/api/token/",
    {
      method: "POST",
      headers: { "content-type": "application/x-www-form-urlencoded" },
      body: "username=restaurant&password=" + password,
    }
  )
    .then((response) => response.text())
    .then((str) => new window.DOMParser().parseFromString(str, "text/xml"))
    .then((dom) => {
      const node = dom.querySelector("UserDetails > Token");
      if (node) return node.textContent;
    });
}

function startSession(token) {
  return fetch(window.config.analyticsHost + "/pi/auth/embeddedTokenLogin", {
    headers: { authorization: "Bearer " + token },
    credentials: "include",
  });
}

var nLoaded = 0;
function handleIframeLoad() {
  nLoaded = nLoaded + 1;

  if (nLoaded === window.config.charts.length) {
    performance.mark("end iframe load");
    var eMessage = document.getElementById("message");
    console.log("loaded", performance.getEntries());
    eMessage.textContent =
      "iFrames initialized in " +
      Math.floor(performance.now() - startTime) +
      "ms";
  }
}
